﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndexWrite.aspx.cs" Inherits="RLM_Tag_Analyzer.IndexWrite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Index Write Data</title>
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="base.css" rel="stylesheet" type="text/css" />
    <telerik:RadStyleSheetManager id="RadStyleSheetManager1" runat="server" />
</head>
<body>
    <form id="form2" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <script type="text/javascript">
        //Put your JavaScript code here.
    </script>
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" UpdateInitiatorPanelsOnly="true">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DataConnection %>"
        SelectCommand="SELECT [indexed]
                          ,[firstWrite]
                          ,[lastWrite]
                          ,[tid]
                          ,[lockState]
                          ,[indexCount]
                          ,[indexMaxRssi]
                          ,[writeCount]
                          ,[writeMaxRssi]
                          ,[writeResult]
                          ,[lockResult]
                      FROM [dbo].[writeTagData]
                      ORDER BY indexed DESC" > 
    </asp:SqlDataSource>
    
    <div>
        <asp:Table runat="server" Width="100%" >
            <asp:TableRow runat="server">
                <asp:TableCell Width="10%" runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" Width="20%" Font-Bold="true" runat="server">
                    <a href="http://127.0.0.1:1880/ui" target="_blank" class="nodeRed">PLC Control (Node-Red)</a>
                </asp:TableCell>
                <asp:TableCell Width="20%" HorizontalAlign="Center" Font-Bold="true" runat="server">
                    <a href="http://127.0.0.1:1880" target="_blank" class="nodeRed">Node-Red Flows</a>
                </asp:TableCell>
                <asp:TableCell Width="20%" HorizontalAlign="Center" Font-Bold="true" runat="server">
                    <a href="default.aspx" class="nodeRed">Validation Trigger/Tag Data</a>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="50%" Font-Bold="true" runat="server">
                    Theme Chooser:&nbsp;&nbsp;
                    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" PersistenceMode="Cookie" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" 
            AllowPaging="true" PageSize="150" Width="100%" AllowFilteringByColumn="true" ShowStatusBar="true" >
            <MasterTableView AutoGenerateColumns="false" AllowSorting="false" CommandItemDisplay="Top">
                <CommandItemSettings ShowAddNewRecordButton="false" />
                <Columns>
                    <telerik:GridBoundColumn DataField="indexed" HeaderText="Indexed" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fffffff }" 
                        ItemStyle-Wrap="false" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" />
                    <telerik:GridBoundColumn DataField="firstWrite" HeaderText="First Write" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fffffff }" 
                        ItemStyle-Wrap="false" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" />
                    <telerik:GridBoundColumn DataField="lastWrite" HeaderText="Last Write" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fffffff }" 
                        ItemStyle-Wrap="false" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" />
                    <telerik:GridBoundColumn DataField="tid" HeaderText="TID" HeaderStyle-VerticalAlign="Bottom"  AllowFiltering="false"  />
                    <telerik:GridBoundColumn DataField="lockState" HeaderText="Lock State" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false">
                    </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="indexCount" HeaderText="Index Count" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false">
                    </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="indexMaxRssi" HeaderText="Index Max RSSI" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false">
                    </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="writeCount" HeaderText="Write Count" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false">
                    </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="writeMaxRssi" HeaderText="Write Max RSSI" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="writeResult" HeaderText="Write Result" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="true" FilterControlWidth="50"
                         CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="lockResult" HeaderText="Lock Result" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="true" FilterControlWidth="50"
                         CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>

        </telerik:RadGrid>
        <hr style="margin-top: 40px" />
        <div class="footer">
            © 2012 eAgile Inc.
            <br />
            <br />
        </div>
    </div>
    </form>
</body>
</html>
