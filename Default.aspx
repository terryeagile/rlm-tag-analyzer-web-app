﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Validation Trigger/Tag Data</title>
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1, maximum-scale=1.0, user-scalable=no" />
    <link href="base.css" rel="stylesheet" type="text/css" />
    <telerik:RadStyleSheetManager id="RadStyleSheetManager1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <script type="text/javascript">
        //Put your JavaScript code here.
    </script>
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" UpdateInitiatorPanelsOnly="true">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtPreSeconds" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtPostSeconds" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DataConnection %>"
        SelectCommand="SELECT CONVERT(nvarchar(30), readerTime, 21) as 'trigger'
                            , [readerTime]
                            , DATEADD(millisecond, @preSeconds, readerTime) as PreTime
	                        , DATEADD(millisecond, @postSeconds, readerTime) as PostTime
                            , DATEDIFF(millisecond, Lag(readerTime, 1) OVER (ORDER BY readerTime asc), readerTime) as time_diff
                            , [tid]            
                            , [decodedSerial]      
                            , [readCount]
                            , [maxRssi]
                            , [flags]
                            , [result]      
                        FROM [dbo].[validationTriggerData]
                        ORDER BY readerTime DESC" > 
        <SelectParameters>
            <asp:ControlParameter Name="preSeconds" Type="Int32" ControlID="txtPreSeconds" />
            <asp:ControlParameter Name="postSeconds" Type="Int32" ControlID="txtPostSeconds" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DataConnection %>"
        SelectCommand="SELECT [readerTime], DATEDIFF(millisecond, Convert(datetime2, @Trigger), readerTime) as TrigDiff, [tid], [rssi]
                        , [antenna], [flags] 
                        FROM [dbo].[validationTagData] 
                        WHERE readerTime >= DATEADD(millisecond, @preSeconds, Convert(datetime2, @Trigger)) 
                            AND readerTime <= DATEADD(millisecond, @postSeconds, Convert(datetime2, @Trigger)) 
                        ORDER BY readerTime ASC">
        <SelectParameters>
            <asp:Parameter Name="Trigger" Type="String" />
            <asp:ControlParameter Name="preSeconds" Type="Int32" ControlID="txtPreSeconds" />
            <asp:ControlParameter Name="postSeconds" Type="Int32" ControlID="txtPostSeconds" />
        </SelectParameters>
</asp:SqlDataSource>

    <div>
        <asp:Table runat="server" Width="100%" >
            <asp:TableRow runat="server">
                <asp:TableCell Width="10%" runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" Width="20%" Font-Bold="true" runat="server">
                    <a href="http://127.0.0.1:1880/ui" target="_blank" class="nodeRed">PLC Control (Node-Red)</a>
                </asp:TableCell>
                <asp:TableCell Width="20%" HorizontalAlign="Center" Font-Bold="true" runat="server">
                    <a href="http://127.0.0.1:1880" target="_blank" class="nodeRed">Node-Red Flows</a>
                </asp:TableCell>
                <asp:TableCell Width="20%" HorizontalAlign="Center" Font-Bold="true" runat="server">
                    <a href="IndexWrite.aspx" class="nodeRed">Write/Index Tag Data</a>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right" Wrap="false" Width="50%" Font-Bold="true" runat="server">
                    Theme Chooser:&nbsp;&nbsp;
                    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" PersistenceMode="Cookie" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow Font-Bold="true" runat="server">
                <asp:TableCell HorizontalAlign="Right" runat="server">
                    Pre-Trigger:&nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" Wrap="false" runat="server">
                    <telerik:RadNumericTextBox RenderMode="Lightweight" ID="txtPreSeconds" runat="server" Value="-150"
                    NumberFormat-DecimalDigits="0" AutoPostBack="true" Width="100" />
                </asp:TableCell>
                <asp:TableCell runat="server" HorizontalAlign="Right" >
                    Post-Trigger:&nbsp;&nbsp;
                </asp:TableCell>
                 <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Wrap="false" runat="server">
                    <telerik:RadNumericTextBox RenderMode="Lightweight" ID="txtPostSeconds" runat="server" Value="150"
                    NumberFormat-DecimalDigits="0" AutoPostBack="true" Width="100" />
                </asp:TableCell>
           </asp:TableRow>
        </asp:Table>
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" 
            AllowPaging="true" PageSize="50" Width="100%" AllowFilteringByColumn="true" ShowStatusBar="true" >
            <MasterTableView AutoGenerateColumns="false" AllowSorting="false" DataKeyNames="trigger" CommandItemDisplay="Top" 
                PagerStyle-Mode="NextPrevNumericAndAdvanced">
                <CommandItemSettings ShowAddNewRecordButton="false" />
                <Columns>
                    <telerik:GridBoundColumn DataField="readerTime" HeaderText="Reader Time" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fffffff }" 
                        ItemStyle-Wrap="false" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" />
                    <telerik:GridNumericColumn DataField="time_diff" HeaderText="Time Since Last Trigger" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" AllowFiltering="false" 
                         HeaderStyle-VerticalAlign="Bottom"/>
                    <telerik:GridDateTimeColumn DataField="PreTime" HeaderText="Pre-Time" DataFormatString="{0:HH:mm:ss.fffffff }"
                         HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" />
                    <telerik:GridDateTimeColumn DataField="PostTime" HeaderText="Post-Time" DataFormatString="{0:HH:mm:ss.fffffff }"
                         HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" />
                    <telerik:GridBoundColumn DataField="tid" HeaderText="TID" HeaderStyle-VerticalAlign="Bottom"  AllowFiltering="false"  />
                    <telerik:GridBoundColumn DataField="decodedSerial" HeaderText="Decoded Serial" HeaderStyle-VerticalAlign="Bottom"  AllowFiltering="false"  />
                    <telerik:GridNumericColumn DataField="readCount" HeaderText="Read Count" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false"   />
                    <telerik:GridNumericColumn DataField="maxRssi" HeaderText="Max RSSI" DataFormatString="{0:00.0}" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false"  />
                    <telerik:GridBoundColumn UniqueName="flags" DataField="flags" HeaderText="Flags" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="false" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="result" DataField="result" HeaderText="Result" ItemStyle-HorizontalAlign="Center" 
                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AllowFiltering="true" FilterControlWidth="50"
                         CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <DetailTables>
                    <telerik:GridTableView runat="server" DataSourceID="SqlDataSource2" AutoGenerateColumns="false" Width="80%" AllowFilteringByColumn="false" >
                        <Columns>
                            <telerik:GridDateTimeColumn DataField="readerTime" HeaderText="Reader Time" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fffffff }" 
                                ItemStyle-Wrap="false" HeaderStyle-VerticalAlign="Bottom" />
                            <telerik:GridNumericColumn DataField="TrigDiff" HeaderText="Trigger Offset" ItemStyle-HorizontalAlign="Center" 
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                 HeaderStyle-VerticalAlign="Bottom"/>
                            <telerik:GridBoundColumn DataField="tid" HeaderText="TID" HeaderStyle-VerticalAlign="Bottom"  />
                            <telerik:GridNumericColumn DataField="rssi" HeaderText="RSSI" DataFormatString="{0:00.0}" ItemStyle-HorizontalAlign="Center" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" />
                            <telerik:GridNumericColumn DataField="antenna" HeaderText="Antenna" ItemStyle-HorizontalAlign="Center" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" />
                             <telerik:GridBoundColumn UniqueName="flags" DataField="flags" HeaderText="Flags" ItemStyle-HorizontalAlign="Center" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" />
                        </Columns>
                        <ParentTableRelation>
                            <telerik:GridRelationFields DetailKeyField="Trigger" MasterKeyField="trigger" />
                        </ParentTableRelation>
                    </telerik:GridTableView>
                </DetailTables>
            </MasterTableView>

        </telerik:RadGrid>
        <hr style="margin-top: 40px" />
        <div class="footer">
            © 2012 eAgile Inc.
            <br />
            <br />
        </div>
    </div>
    </form>
</body>
</html>
